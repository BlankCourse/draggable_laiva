import Navbar from './Navbar';
import Field  from './Field';
import Footer from './Footer';
import Spacer from './Spacer';
import Ship from './Ship';
import OpponentField from './OpponentField';
import React from 'react';
import styles from '../../src/Style.module.css';




const Layout = (props) => (

    <div>
        
        {/*Vaihtaa sivun title*/}

            <title>Laivanupotus</title>
        
        {/*Bootstrap*/}
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous"/>
        {/*jquery*/}
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        {/*javascript*/}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        {/*style sheet*/}
        <link rel="stylesheet" href="App.css" type="text/css"/>

        


         {/*Header*/}
    <body className={styles.body}>
        <main className={styles.main}> 

    
            <Navbar/> 
            <Spacer/> {/*blokki millä asetellut sivun elementit paremmin*/}
            

          
            <div className="container"> 
                
                <div className="row">
                    <h1 className="col">Pelaaja 1</h1>
                    <h1 className="col">Pelaaja 2</h1>
                </div>

      
               
                <div className="row">   {/*asettaa kentät riviin*/}
                    
                    <div className="col">
                        <Field/> {/*pelikenttä*/} 
                    </div>
                   
                    <div className="col">
                        <OpponentField/> {/*pelikenttä*/} 
                    </div>
                </div>

            </div>
            <Spacer/>
            <Ship/>
         
            
            
          

            <Spacer/>
            <Spacer/>
            <Spacer/>
        
            <Footer/>

        </main>
    </body>
    </div>
);

export default Layout;