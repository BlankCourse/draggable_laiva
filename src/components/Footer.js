import React from 'react';


const Footer = () => (
<div>

<footer className="page-footer font-small unique-color-dark pt-4 bg-dark">
              
              <div className="container ">  
                <ul className="list-unstyled list-inline text-center py-2">
                  <li className="list-inline-item">
                    <h5 className="mb-1 text-light">Footer</h5>
                  </li>
                  <li className="list-inline-item text-light">
                    <p>testi</p>
                  </li>
                </ul>
              </div>
    </footer>
          
</div>

);

export default Footer;