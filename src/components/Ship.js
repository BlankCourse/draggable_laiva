import React from 'react';
import Draggable from 'react-draggable';

export default class Ship extends React.Component {
  //on drop event for ships -> change state?
  dropEvent = () => {
    console.log("drag stop");
  }

  state = {
    activeDrags: 0,
    deltaPosition: {
      x: 0, y: 0
    }
  };

  handleDrag = (e, ui) => {
    const { x, y } = this.state.deltaPosition;
    this.setState({
      deltaPosition: {
        x: x + ui.deltaX,
        y: y + ui.deltaY,
      }
    });
  };



  constructor() {
    super();

    this.ships = [
      {name: 'lentotukialus', length: 5, id: 5},
      {name: 'taistelulaiva', length: 4, id: 4},
      {name: 'sukellusvene', length: 3, id: 3},
      {name: 'silja line', length: 3, id: 2},
      {name: 'Vene', length: 2, id: 1},
    ];
    
  }


  render() {
    const { deltaPosition } = this.state;
    return (
      
      <div className = "container">
         
        <p>Valitse sijoitettava laiva</p>
       
        <div>
        {this.ships.map(ship => {
          
           return (
            <Draggable onStop={this.dropEvent} onDrag={this.handleDrag}>
              
              <button key={ship.name} value={ship.name} id={ship.id} >
                {`${ship.name}`}
              </button>
            </Draggable>
          ) 
        })}
         </div>
         

      </div>
      
    )
  }
};